from functools import reduce

list_of_characters = [
    {
        "id": 1,
        "name": "huck",
        "intelligence": 9,
        "power": 7,
        "strength": 10,
        "agility": 8
    },
    {
        "id": 2,
        "name": "super man",
        "intelligence": 10,
        "power": 10,
        "strength": 10,
        "agility": 10
    }
]


def characters_filter(list_of_characters, key, value):
    obj = list(filter(lambda x: x[key] == value, list_of_characters))
    return obj

def team_power(list_of_characters):
    result = sum(map(lambda x: x['power'], list_of_characters))
    return result

def team_power_2(list_of_characters, advantage):
    result = reduce(lambda x, y: x + y['power'], list_of_characters, advantage)
    return result
